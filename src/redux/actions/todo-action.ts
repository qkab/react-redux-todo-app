import { ITodo, TodoActions } from "../../interfaces/todo-interface";
import { v4 as uuidV4} from 'uuid'

export const addTodo = (todo: Partial<ITodo>) => {
    return (dispatch: any) => {
        dispatch({ type: TodoActions.ADD_TODO, payload: { ...todo, id: uuidV4() } })
    }
}