import { ITodoState, TodoActions } from "../../interfaces/todo-interface"

const initialState: ITodoState = {
    todos: []
}

export default function(state = initialState, action: any){
    switch(action.type) {
        case TodoActions.ADD_TODO:
            return {
                ...state,
                todos: [...state.todos, action.payload]
            }
        case TodoActions.UPDATE_TODO:
            return {
                ...state,
                todos: state.todos.map((todo) => {
                    if(todo.id === action.payload.id) {
                        return action.payload
                    }
                    return todo
                })
            }
        case TodoActions.DELETE_TODO:
            return {
                ...state,
                todos: state.todos.filter((todo) => todo.id !== action.payload)
            }
        default:
            return state
    }
}