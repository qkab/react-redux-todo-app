import React from 'react'
import { ITodo } from '../interfaces/todo-interface'
interface ITodoProps {
    todo: ITodo
}
const Todo: React.FC<ITodoProps> = ({ todo }) => {
    return (
        <div className='card'>
            <h3>{todo.title}</h3>
            <p>{todo.description}</p>
            {todo.completed ? (
                <h6>It is completed</h6>
            ) : (
                <h6>It isn't completed</h6>
            )}
        </div>
    )
}

export default Todo
