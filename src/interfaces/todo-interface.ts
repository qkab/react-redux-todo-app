import { DefaultRootState } from "react-redux";

export interface ITodo {
    id: string
    title: string
    description: string
    completed: boolean
}
export interface ITodoState extends DefaultRootState {
    todos: ITodo[]
}

export enum TodoActions {
    ADD_TODO = 'ADD_TODO',
    UPDATE_TODO = 'UPDATE_TODO',
    DELETE_TODO = 'DELETE_TODO'
}