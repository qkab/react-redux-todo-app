import React, { useState } from 'react';
import './App.css';
import { useDispatch, useSelector } from 'react-redux';
import Todo from './components/Todo';
import { ITodo } from './interfaces/todo-interface';
import { bindActionCreators } from '@reduxjs/toolkit';
import actionsCreators from './redux/actions'

function App() {
  const [title, setTitle] = useState('')
  const [description, setDescription] = useState('')
  const { todos } = useSelector((state: any) => state.todos)
  const dispatch = useDispatch()
  const { addTodo } = bindActionCreators(actionsCreators, dispatch)

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    const todo = { title, description, completed: false }
    addTodo(todo)
    setTitle('')
    setDescription('')
  }
  return (
    <div className="App">
      <h1>Hello World</h1>
      <h6>Number of todos: {todos.length}</h6>
      <div className="todos">
        {todos.map((todo: ITodo) => <Todo key={todo.id} todo={todo} />)}
      </div>
      <br />
      {/* Todo FORM */}
      <div>
        <form className='form' onSubmit={handleSubmit}>
          <label htmlFor="title">Title</label>
          <input type="text" id='title' name='title' value={title} onChange={(e) => setTitle(e.target.value)} />
          <label htmlFor="description">Description</label>
          <textarea id='description' name='description' value={description} onChange={(e) => setDescription(e.target.value)} cols={30} rows={10}></textarea>
          <button type="submit">Add todo</button>
        </form>
      </div>
    </div>
  );
}

export default App;
